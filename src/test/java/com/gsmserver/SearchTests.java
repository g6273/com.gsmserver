package com.gsmserver;

import com.codeborne.selenide.CollectionCondition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class SearchTests {

    @BeforeEach
    void openHomePage() {
        open("https://gsmserver.com/");
    }

    @Test
    void searchProductByTitleAndAddToCart() {

        var productName = "XTC 2 Clip with Power Adapter";
        var productID = "871442";

        $("[name='searchword']").val(productName).pressEnter();
        $("[key='list']").click();
        $(".col-12.col-md-8.col-lg-9").shouldHave(text(productName));
        $("[space*='list']").shouldHave(text(productID));
        $(".component_product_list_product-info_item.info-id").shouldHave(text(productID));
        $(".btn--add-to-cart").click();
        $("[href*='cart']").click();  //or $(byText("Cart")).click();
        $$(".component_product_table.product-table.pdt.-removable-items.-quantity-button").shouldHave(CollectionCondition.size(1));
        $(".pr-tiny_title").shouldHave(text(productName));
    }

    @Test
    void searchProductByTitleTest() {
        var productName = "XTC 2 Clip with Power Adapter";
        new HomePage().searchFor(productName);
        var actualSearchResultTitle = new searchResultPage().getSearchResultTitle(productName);
        Assertions.assertEquals(productName, actualSearchResultTitle);
    }
}